import React, {Component} from "react";
import axios from 'axios';

import './MontyHall.css';


class Form extends Component {
    state = {
        simulations: '',
        change: '',
        sim_simulation: '',
        sim_change: '',
        wins: ''
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleRadioChange = (changeEvent) => {
        this.setState({
            change: changeEvent.target.value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const {simulations, change} = this.state;
        axios.post(`simulate`, {simulations, change})
            .then(res => {
                if (res.status === 200) {
                    this.setState({
                        wins: res.data.wins,
                        sim_simulation: res.data.simulationParameters.simulations,
                        sim_change: res.data.simulationParameters.change
                    })
                } else {
                    this.setState({
                        wins: "Unexpected result"
                    })
                }
            })
            .catch(res => {
                console.log(res)
            });
    }

    render() {
        return (
            <div className="MontyHall">
                <h2>Simulation parameters</h2>
                <form onSubmit={this.handleSubmit}>
                    <label className="label">
                        Number of simulations:
                        <input name="simulations" type="number" value={this.state.simulations} onChange={this.handleChange} />
                    </label>
                    <br/>

                    <div className="radio">
                        <label className="label">
                            <input type="radio" value="false" onChange={this.handleRadioChange} checked={this.state.change === 'false'} />
                            Keep box
                        </label>
                        <label className="label">
                            <input type="radio" value="true" onChange={this.handleRadioChange} checked={this.state.change === 'true'} />
                            Change box
                        </label>
                    </div>
                    <br/>
                    <button type="submit">Simulate!</button>
                </form>

                <p><h2>Result</h2>
                    Win factor {this.state.wins}/{this.state.sim_simulation} when changing boxes was set to {this.state.wins === '' ? '' : this.state.sim_change ? "true" : "false"}
                </p>
            </div>
        );
    }
}


class MontyHall extends Component {

    constructor(props) {
        super(props);
        this.state = {
            wins : '',
            simulations : '',
            change: ''
        };
    }

    render() {
        return (
            <div className="MontyHallSimulation">
                <Form />
            </div>
        );
    }
}

export default MontyHall;