package se.comhem.test.montyhall.model;


public class SimulationParameters {
    private Long simulations;
    private Boolean change;

    public Long getSimulations() {
        return simulations;
    }
    public void setSimulations(Long simulations) {
        this.simulations = simulations;
    }

    public Boolean getChange() {
        return change;
    }
    public Boolean isChange() {
        return change;
    }
    public void setChange(Boolean change) {
        this.change = change;
    }
}