package se.comhem.test.montyhall.model;

public class Data {
    private String status;
    private Long wins;
    private SimulationParameters simulationParameters;

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public Long getWins() {
        return wins;
    }
    public void setWins(Long wins) {
        this.wins = wins;
    }

    public SimulationParameters getSimulationParameters() {
        return simulationParameters;
    }
    public void setSimulationParameters(SimulationParameters simulationParameters) {
        this.simulationParameters = simulationParameters;
    }
}
