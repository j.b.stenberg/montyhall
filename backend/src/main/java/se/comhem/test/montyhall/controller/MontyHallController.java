package se.comhem.test.montyhall.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.comhem.test.montyhall.MontyHallGame;
import se.comhem.test.montyhall.model.Data;
import se.comhem.test.montyhall.model.SimulationParameters;

@RestController
public class MontyHallController {

    @Autowired
    MontyHallGame montyHallGame;

    @RequestMapping("/health")
    public ResponseEntity<?> healthCheck() {
        Data data = new Data();
        data.setStatus("UP");
        return new ResponseEntity<Data>(data, HttpStatus.OK);
    }

    @PostMapping("/simulate")
    public ResponseEntity<?> simulateMontyHall(@RequestBody SimulationParameters simulationParameters) {
        long wins = 0L;

        for (int i = 0; i < simulationParameters.getSimulations(); i++) {
            wins = montyHallGame.MontyHallWinner(simulationParameters.isChange()) ? ++wins : wins;
        }

        Data data = new Data();
        data.setWins(wins);
        data.setSimulationParameters(simulationParameters);
        return new ResponseEntity<Data>(data, HttpStatus.OK);
    }
}