package se.comhem.test.montyhall;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class MontyHallGame {
    int prize = 0;
    int choice = 0;

    public boolean MontyHallWinner(Boolean change) {
        prize = theBox();
        choice = theBox();
        int win = 0;

        return change ? prize != choice : prize == choice;
    }


    private int theBox() {
        return new Random().nextInt(3) +1;
    }
}
